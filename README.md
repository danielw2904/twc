# Temporal Walk Centrality
Source code for our paper *"Temporal Walk Centrality: Ranking Nodes in Evolving Networks"*.


## Build

For compilation you need the [Eigen3 library](https://eigen.tuxfamily.org/).
Set the environment variable `EIGEN3_INCLUDE_DIR` to the Eigen3 folder.
For example:

    export EIGEN3_INCLUDE_DIR=/home/user/Downloads/eigen-3.3.7


In the folder `source/twc` run:
```
mkdir cmake-build-release
cd cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

After compilation running `twc` shows options and help.


## Data Sets
 
The folder `datasets` contains the example data set.

The format for the temporal graphs is the following: 
First line states number of nodes. Each following line of 
the format `u v t 1` represents a temporal edge (u,v,t) from node u to v at
time t.

## Terms and conditions
If you use our code please cite:

```
@inproceedings{Oettershagen_twc_2022,
    title={Temporal Walk Centrality: Ranking Nodes in Evolving Networks},
    author={Lutz Oettershagen and Petra Mutzel and Nils M. Kriege},
    booktitle = {{WWW} '22: The Web Conference 2022},
    pages={},
    year={2022}
}
```
