#ifndef CENTRALITY_EXPERIMENTS_H
#define CENTRALITY_EXPERIMENTS_H

#include "TemporalGraph/TemporalGraphs.h"
#include "Helpers/Params.h"

void compute_centrality(Params &params, TemporalGraphStream tgs, TemporalGraph tg);

#endif //CENTRALITY_EXPERIMENTS_H
