#include "Params.h"
#include "SGLog.h"

using namespace std;


void show_help() {
    cout << "Usage: twc [dataset] [mode]" << endl;
    cout << "Optional: [-k=top-k] [-o=outfile] [-c=algorithm] "
            "[-a=alpha] [-d=delta] [-u=undirected] [-e=epsilon]" << endl;
    cout << "Modes:" << endl;
    cout << comp_centrality << "\t compute centrality" << endl;
    cout << tg_statistics << "\t show tg statistics" << endl;
    cout << approx_accuracy << "\t compute approx and errors" << endl;
    cout << "\n Algorithms:" << endl;
    cout << TWC_Stream << "\t Stream" << endl;
    cout << TWC_Stream_Time << "\t Stream (time)" << endl;
    cout << TWC_Matrix << "\t DLGMa" << endl;
    cout << TWC_Matrix_Approx << "\t Approx" << endl;
    cout << TWC_Matrix_NonSparse << "\t DLGMa (non-sparse)" << endl;

    exit(0);
}

bool Params::parseArgs(vector<string> args) {
    if (args.size() < 2) {
        return false;
    }
    for (auto & arg : args) {
        SGLog::log() << arg << " ";
    }
    SGLog::log() << endl;

    try {
        dataset_path = args[0];
        mode = stoi(args[1]);
    } catch (...) {
        return false;
    }

    for (size_t i = 2; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-c=") {
            string valstr = next.substr(3);
            centrality = static_cast<Centrality>(stoi(valstr));
            SGLog::log() << "set centrality " << centrality << endl;
        } else if (next.substr(0, 3) == "-y=") {
            string valstr = next.substr(3);
            random_seed = stoi(valstr);
            SGLog::log() << "set random seed " << random_seed << endl;
        } else if (next.substr(0, 3) == "-e=") {
            string valstr = next.substr(3);
            epsilon = stod(valstr);
            SGLog::log() << "set epsilon " << epsilon << endl;
        } else if (next.substr(0, 3) == "-k=") {
            string valstr = next.substr(3);
            k = stoul(valstr);
            SGLog::log() << "set k " << k << endl;
        } else if (next.substr(0, 3) == "-d=") {
            string valstr = next.substr(3);
            delta = stoul(valstr);
            SGLog::log() << "set delta " << delta << endl;
        } else if (next.substr(0, 3) == "-o=") {
            string valstr = next.substr(3);
            result_filename = valstr;
            SGLog::log() << "set result filename " << result_filename << endl;
        } else if (next.substr(0, 3) == "-a=") {
            string valstr = next.substr(3);
            alpha = stod(valstr);
            beta = alpha;
            SGLog::log() << "set alpha " << alpha << endl;
        } else if (next.substr(0, 3) == "-u=") {
            string valstr = next.substr(3);
            directed = stoi(valstr) == 0;
            SGLog::log() << "set directed " << directed << endl;
        }
        else {
            return false;
        }
    }
    return true;
}
