#include "TemporalGraphs.h"
#include <string>

using namespace std;

void TGNode::addEdge(TemporalEdge e) {
    adjlist.push_back(e);
    if (e.t < minTime) minTime = e.t;
    if (e.t > maxTime) maxTime = e.t;
}

TemporalGraph TemporalGraphStream::toTemporalGraph() {
    TemporalGraph g;

    for (unsigned int i = 0; i < num_nodes; ++i) {
        TGNode node;
        node.id = i;
        node.done = false;
        g.nodes.push_back(node);
        g.num_nodes++;
    }

    for (auto &e: edges) {
        g.nodes.at(e.u_id).addEdge(e);
        if (e.t > g.nodes.at(e.u_id).maxTime) g.nodes.at(e.u_id).maxTime = e.t;
        if (e.t < g.nodes.at(e.u_id).minTime) g.nodes.at(e.u_id).minTime = e.t;
    }
    g.num_edges = edges.size();

    g.label = label;

    Time minD = MAX_UINT_VALUE;
    for (auto &n: g.nodes) {
        for (auto &e: n.adjlist) {
            Time arr = e.t + e.traversal_time;
            NodeId w = e.v_id;
            Time d = MAX_UINT_VALUE;
            for (auto &f: g.nodes.at(w).adjlist) {
                if (f.t >= arr) {
                    if (d > f.t - arr)
                        d = f.t - arr;
                }
            }
            if (minD > d)
                minD = d;
        }
    }
    if (minD == MAX_UINT_VALUE)
        minD = 0;
    cout << "min delta: " << minD << endl;
    g.minTimeDelta = minD;

    return g;
}

void TemporalGraphStream::sort_edges() {
    std::sort(edges.begin(), edges.end(),
              [](const TemporalEdge &a, const TemporalEdge &b) -> bool { return a.t < b.t; });
}

TemporalEdges TemporalGraphStream::reduce() {
    TemporalEdges tes;
    std::stable_sort(edges.begin(), edges.end(), [](const TemporalEdge &a, const TemporalEdge &b) -> bool {
        return a.v_id < b.v_id;
    });
    std::stable_sort(edges.begin(), edges.end(), [](const TemporalEdge &a, const TemporalEdge &b) -> bool {
        return a.u_id < b.u_id;
    });
    TemporalEdge le;
    for (TemporalEdge &e: edges) {
        if (e.v_id != le.v_id || e.u_id != le.u_id) {
            le = e;
            tes.push_back(e);
        }
    }
    return tes;
}

std::ostream &operator<<(std::ostream &os, const TemporalGraphStream &tgs) {
    std::string s1 = "num_nodes\t" + std::to_string(tgs.num_nodes) + "\n";
    std::string s2 = "num_edges\t" + std::to_string(tgs.edges.size()) + "\n";
    std::string s3 = "max_time\t" + std::to_string(tgs.get_max_time()) + "\n";

    return os << s1 << s2 << s3;
}

std::ostream &operator<<(std::ostream &os, const TemporalEdge &tgs) {
    std::string s1 = std::to_string(tgs.u_id) + " " + std::to_string(tgs.v_id) + " " + std::to_string(tgs.t) + " " +
                     std::to_string(tgs.traversal_time) + " " + "\n";

    return os << s1;
}
