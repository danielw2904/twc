#ifndef TGKERNEL_GRAPHDATALOADER_H
#define TGKERNEL_GRAPHDATALOADER_H

#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <unordered_set>
#include <unordered_map>

using NodeId = unsigned int;
using EdgeId = unsigned int;
using Time = unsigned long;
using Cost = float;
using Label = long;

struct TemporalEdge;
struct TGNode;
using AdjacenceList = std::vector<TemporalEdge>;

static constexpr auto MAX_UINT_VALUE = std::numeric_limits<int>::max();

struct TGNode {
    NodeId id = 0;

    Time minTime = MAX_UINT_VALUE;
    Time maxTime = 0;

    void addEdge(TemporalEdge t);

    AdjacenceList adjlist;

    bool done = false;

};


struct TemporalEdge {
    TemporalEdge() = default;

    TemporalEdge(NodeId u, NodeId v, Time time, Cost c, Time tt, EdgeId eid) : u_id(u), v_id(v), t(time), cost(c),
                                                                               traversal_time(tt), id(eid),
                                                                               next(nullptr) {}

    TemporalEdge(NodeId u, NodeId v, Time time, EdgeId eid) : u_id(u), v_id(v), t(time), cost(0),
                                                              traversal_time(1), id(eid), next(nullptr) {}

    TemporalEdge(NodeId u, NodeId v, Time time) : u_id(u), v_id(v), t(time), cost(0),
                                                  traversal_time(1), id(0), next(nullptr) {}

    NodeId u_id{}, v_id{};
    Time t{};
    Cost cost{};
    Time traversal_time{};

    bool deleted = false;

    bool operator()(TemporalEdge const *lhs, TemporalEdge const *rhs) const {
        return lhs->u_id == rhs->u_id && lhs->v_id == rhs->v_id && lhs->t == rhs->t;
    }

    EdgeId id{};

    TemporalEdge *next = nullptr;

    TemporalEdge(const TemporalEdge &e) : u_id(e.u_id), v_id(e.v_id), t(e.t), cost(e.cost),
                                          traversal_time(e.traversal_time), id(e.id), next(e.next), values(e.values),
                                          rvalues(e.rvalues) {}


    std::vector<int> values;
    std::vector<double> rvalues;

};


using TGNodes = std::vector<TGNode>;
using TemporalEdges = std::vector<TemporalEdge>;

struct TemporalGraphStream;

struct TemporalGraph {
    TemporalGraph() : num_nodes(0), num_edges(0), label(0), nodes(TGNodes()), minTimeDelta(0) {};

    TemporalGraph(const TemporalGraph &tg) {
        for (TGNode const &n: tg.nodes) {
            TGNode nn;
            nn.minTime = n.minTime;
            nn.maxTime = n.maxTime;
            nn.id = n.id;
            nn.done = n.done;
            for (auto &e: n.adjlist) {
                nn.adjlist.push_back(e);
            }
            nodes.push_back(nn);
        }

        num_edges = tg.num_edges;
        num_nodes = tg.num_nodes;
        label = tg.label;
        minTimeDelta = tg.minTimeDelta;
    }

    unsigned int num_nodes;
    unsigned int num_edges;
    Label label;
    TGNodes nodes;
    Time minTimeDelta;
};


struct TemporalGraphStream {
    unsigned long num_nodes;
    Label label;
    TemporalEdges edges;

    [[nodiscard]] Time get_max_time() const {
        if (edges.empty()) return 0;
        return edges.back().t;
    }

    void sort_edges();

    TemporalGraph toTemporalGraph();

    TemporalEdges reduce();
};


std::ostream &operator<<(std::ostream &os, const TemporalGraphStream &tgs);

std::ostream &operator<<(std::ostream &os, const TemporalEdge &tgs);

#endif //TGKERNEL_GRAPHDATALOADER_H
