#ifndef DIRECTEDLINEGRAPH_H
#define DIRECTEDLINEGRAPH_H

#include "TemporalGraphs.h"
#include "Helpers/Params.h"
#include "Helpers/ResultFile.h"
#include <map>

struct DLEdge {
    NodeId u{};
    NodeId v{};
    NodeId tg_u{};
    NodeId tg_v{};
    NodeId tg_w{};
    Time u_t{};
    Time v_t{};
    std::vector<unsigned long> walks;
    [[nodiscard]] DLEdge getInverseEdge() const {
        DLEdge e;
        e.u_t = v_t;
        e.u = v;
        e.tg_u = tg_w;
        e.v_t = u_t;
        e.v = u;
        e.tg_w = tg_u;
        e.tg_v = tg_v;
        e.walks = walks;
        return e;
    }
};

struct DLNode {
    NodeId id;
    TemporalEdge e;
    std::vector<std::shared_ptr<DLEdge>> inedges;
    std::vector<std::shared_ptr<DLEdge>> outedges;
};

struct DirectedLineGraph {
    unsigned long num_tg_nodes{};
    unsigned long num_edges{};
    std::vector<DLNode> nodes;
    bool reversed = false;
};

DirectedLineGraph temporalGraphToDirectedLineGraph(TemporalGraphStream const &tgs, TemporalGraph const &tg);

DirectedLineGraph reverseDLG(DirectedLineGraph const &dlg);

#endif //DIRECTEDLINEGRAPH_H
