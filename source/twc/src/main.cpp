#include <TemporalGraph/TemporalGraphStreamProvider.h>
#include <Helpers/SGLog.h>
#include "TemporalGraph/TemporalGraphs.h"
#include <iomanip>
#include <cmath>
#include <Helpers/AccuracyTest.h>
#include "Helpers/Params.h"
#include "Helpers/HelperFunctions.h"
#include "Experiments.h"

using namespace std;

void run_experiment(Params &params, TemporalGraphStream &tgs, TemporalGraph &tg) {
    switch (params.mode) {
        case comp_centrality : {
            compute_centrality(params, tgs, tg);
            break;
        }
        case tg_statistics : {
            auto tgstats = HF::getTemporalGraphStreamsStatistics(tgs);
            SGLog::log() << tgstats;
            break;
        }
        case approx_accuracy : {
            test_accuracy(tgs, tg, params);
            break;
        }
        default :
            show_help();
    }
}


int main(int argc, char *argv[]) {

    srand((unsigned) time(nullptr));

    vector<string> args;
    for (size_t i = 1; i < argc; ++i)
        args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args))
        show_help();


    if (params.random_seed != 0)
        srand(params.random_seed);
    else {
        auto seed = rand();
        SGLog::log() << "random seed: " << seed << endl;
        srand(seed);
    }
    std::cout << std::setprecision(10);

    TemporalGraphStreamProvider p;
    p.loadTemporalGraph(params.dataset_path, params.directed);

    SGLog::log() << "Loading " << params.dataset_path << endl;

    SGLog::log() << "#nodes: " << p.getTGS().num_nodes << endl;
    SGLog::log() << "#edges: " << p.getTGS().edges.size() << endl;

    run_experiment(params, p.getTGS(), p.getTG());

    return 0;
}