#ifndef TGPR_WALKCENTRALITYSTREAMNEWVERSION_H
#define TGPR_WALKCENTRALITYSTREAMNEWVERSION_H

#include "../TemporalGraph/TemporalGraphs.h"
#include "../Helpers/ResultFile.h"

Result calculateWalkCentralityStreamNewVersion(TemporalGraphStream const &tgs, Params const &params);

#endif //TGPR_WALKCENTRALITYSTREAMNEWVERSION_H
